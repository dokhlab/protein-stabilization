# INSTALLATION

1. System Requirements:

    The pipeline only works on Linux 64bit.

2. Install Anaconda

    Please download Anaconda from: https://www.anaconda.com/products/individual, and then install it according to the official manual.

3. Install PyMOL

        conda install -c schrodinger pymol

4. Install MDAnalysis

        conda install -c conda-forge mdanalysis

5. Install this package

        git clone https://bitbucket.org/dokhlab/protein-stabilization.git
        cd protein-stabilization
        install_path=$(pwd)
        echo "export PROSTAB_HOME=$install_path" >>~/.bashrc
        echo 'export PATH=$PROSTAB_HOME/bin:$PATH' >>~/.bashrc
        . ~/.bashrc

# USAGE

1. Initialize a new working enviroment

        mkdir test
        cd test
        cp $PROSTAB_HOME/scripts/* .

2. Prepare the input pdb file

        cp /path/to/the/pdb/file input.pdb

3. Stage 1 of the pipeline

        bash run1.sh

4. Select mutation site according to conservation score, SASA, and RMSF

    All mutations should be put in a file named 'muts'.
    Each line of the 'muts' file corresponds to a mutation.
    A mutation could be a single mutation, double mutation, triple mutation, or more...
    An example 'muts' file is as below:

        Y21A,L25H
        E36C
        A51Y,Q61S,E67D,I68G
        L75E
        Q81K

5. Stage 2 of the pipeline

        bash run2.sh

