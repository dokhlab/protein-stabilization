import sys
from Bio.PDB import *
import numpy as np

in_file = sys.argv[1]
rmsf_file = sys.argv[2]
out_file = sys.argv[3]

parser = PDBParser()
pdb = parser.get_structure('obj', in_file)

rmsf = np.genfromtxt(rmsf_file)
rmsf /= np.max(rmsf)

for model in pdb:
    i = 0
    for chain in model:
        for residue in chain:
            for atom in residue:
                atom.set_bfactor(rmsf[i])
            i += 1

io = PDBIO()
io.set_structure(pdb)
io.save(out_file)
