import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import os
import sys
import re
from matplotlib import cm

name_file = sys.argv[1]
ddg_file = sys.argv[2]
#fig_file = sys.argv[3]

cmap = cm.get_cmap('RdYlGn')

names = []
for line in open(name_file):
    l = line.strip()
    if len(l) > 0:
        names.append(l)

names = [re.split(',', i)[0] for i in names]

data = np.genfromtxt(ddg_file)
avgs = data.T[0]
err = data.T[1]

avg_norm = []
for i in avgs:
    if i > 0:
        avg_norm.append(-i/30.0/2.0+0.5)
    else:
        avg_norm.append(-i/15.0/2.0+0.5)
colors = [cmap(i) for i in avg_norm]

n = len(names)

plt.ylim(-20, 50)
plt.xlabel('Mutation')
plt.ylabel(u"\u0394\u0394G (kcal/mol)")
plt.xticks(range(n), labels=names, rotation=90)
plt.bar(range(n), avgs, color=colors, yerr=err, ecolor="grey", capsize=2)
plt.tight_layout()
#plt.savefig(fig_file)
plt.show()
