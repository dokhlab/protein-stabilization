DMD_HOME=$PROSTAB/dmd
export PATH=$DMD_HOME/bin:$PATH

echo "NOTE: Please make sure that you have already prepared the 'input.pdb' file."

# calculate conservation score
calc_conservation() {
    python consurf.py input.pdb
    unzip consurf.zip -d consurf
    perl -lane 'print $F[4] if /\d+\s+(\w+)\s+(\w+):(\w+)\s(.+)\s+(\d+)\s+/' consurf/consurf.grades >conservation.txt
    python bfactor.py input.pdb conservation.txt conservation.pdb
}

# calculate sasa
calc_sasa() {
    python index.py input.pdb >input.ind
    bash area.sh input.pdb input.ind sasa.txt
    python bfactor.py input.pdb sasa.txt sasa.pdb
}

# calculate rmsf
calc_rmsf() {
    bin/complex.linux -P $DMD_HOME/parameter -I input.pdb -D 300 -p param -s state
    pdmd.linux -i start.x -p param -s state -m 16
    complex_M2P.linux $DMD_HOME/parameter input.pdb /dev/null movie traj.pdb
    perl -i -ane '$s.=$_;if(/ENDMDL/){print "MODEL\n".$s;$s=""}' traj.pdb
    python align-movie.py input.pdb traj.pdb aligned.pdb
    python rmsf.py aligned.pdb rmsf.txt rmsf.png
    python bfactor.py input.pdb rmsf.txt rmsf.pdb
}

calc_conservation
calc_sasa
calc_rmsf

echo Congratulations! The 1st stage of the pipeline has now been finished. Please check the Conservation Score, SASA, and RMSF in the 'conservation.txt', 'sasa.txt', and 'rmsf.txt', respectively. We have also assigned the Conservation Score, SASA, and RMSF values in the b-factor column of the PDB files 'conservation.pdb', 'sasa.pdb', and 'rmsf.pdb', respectively. Please choose the mutation sites and then run the 'run2.sh' for the 2nd stage of the pipeline.
