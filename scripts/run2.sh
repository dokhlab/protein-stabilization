ERIS_HOME=$PROSTAB/eris
export PATH=$ERIS_HOME/bin:$PATH

WORK=eris_working

echo NOTE: Please make sure that you have prepared the 'input.pdb' file and the 'muts' file.

eris() {
    INPUT=$1
    MUTATION=$2
    MC_STEP=$3
    SEED=$4

    CWD=$(pwd)

    mkdir -p $WORK

    dir=${WORK}/${MUTATION}_${SEED}
    mkdir -p $dir
    cp $INPUT $dir/input.pdb
    cd $dir

    mut=$(perl -e 'print join(" ",map {$_=~s/([A-Z])(\d+)([A-Z])/$1 $2 $3/g;$_} split(/,/,"'$MUTATION'"))')
    echo $mut
    getContactSC.linux $ERIS_HOME/parameter input.pdb 10 0 $mut

    mkdir -p nat
    cd nat
    complex_fixbb_customDesign.linux -p $ERIS_HOME/parameter -i ../input.pdb -o nat -n 1 -l $MC_STEP -t ../native.dt -s $SEED >log.txt
    cd ..

    mkdir -p mut
    cd mut
    complex_fixbb_customDesign.linux -p $ERIS_HOME/parameter -i ../input.pdb -o mut -n 1 -l $MC_STEP -t ../mutation.dt -s $SEED >log.txt
    cd ..

    cd $CWD
}

echo 'Eris calculation...'
for mut in $(cat muts); do
    echo -ne "  "$mut": "
    for i in $(seq 1 50); do
        echo -ne ${i}"\t"
        eris input.pdb mut 20 ${i}
    done
    echo
done

echo 'Calculate the free energy changes of all mutations...'
for mut in $(cat muts); do
    python ddg.py ${mut}
done >ddg.txt

echo 'Plot all the free energy changes'
python plot-ddg.py muts ddg.txt
