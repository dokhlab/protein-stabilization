import matplotlib.pyplot as plt
import numpy as np
import sys
from Bio.PDB import *

ref_file = sys.argv[1]
traj_file = sys.argv[2]
out_file = sys.argv[3]

parser = PDBParser()
sup = Superimposer()

ref = parser.get_structure('ref', ref_file)

### All-atom RMSD
#ref_atoms = [atom for atom in ref.get_atoms()]

### CA RMSD
ref_atoms = [res['CA'] for res in ref.get_residues()]

traj = parser.get_structure('traj', traj_file)
n = 1
for snapshot in traj:
### All-atom RMSD
#    snapshot_atoms = [atom for atom in snapshot.get_atoms()]

### CA RMSD
    snapshot_atoms = [res['CA'] for res in snapshot.get_residues()]
    sup.set_atoms(ref_atoms, snapshot_atoms)
    sup.apply(snapshot.get_atoms())
    print('FRAME {}'.format(n))
    sys.stdout.flush()
    n += 1

io = PDBIO()
io.set_structure(traj)
io.save(out_file)
