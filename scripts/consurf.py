import requests
import sys
import re
import time

def download_file(url, local_filename):
#    local_filename = url.split('/')[-1]
    with requests.get(url, stream=True) as r:
        r.raise_for_status()
        with open(local_filename, 'wb') as f:
            for chunk in r.iter_content(chunk_size=8192): 
                f.write(chunk)
    return local_filename

pdb_file = sys.argv[1]

url = 'https://consurf.tau.ac.il/cgi-bin/consurf.cgi'
files = {
    'Run_Number': (None, 'NONE'),
    'DNA_AA': (None, 'AA'),
    'PDB_yes_no': (None, 'yes'),
    'pdb_ID': (None, '2vaa'),
    'MAX_FILE_SIZE': (None, '2000000'),
    'pdb_FILE': (pdb_file, open(pdb_file, 'rb'), 'chemical/x-pdb'),
    'PDB_chain': (None, 'A'),
    'msa_FILE_ajax': (None, ''),
    'MAX_FILE_SIZE': (None, '300000'),
    'msa_SEQNAME_ajax': (None, ''),
    'msa_FILE': (None, ''),
    'msa_SEQNAME': (None, ''),
    'tree_FILE': (None, ''),
    'MSA_yes_no': (None, 'no'),
    'MSAprogram': (None, 'MAFFT'),
    'proteins_DB': (None, 'SWISS-PROT'),
    'MAX_NUM_HOMOL': (None, '50'),
    'MAX_REDUNDANCY': (None, '95'),
    'MIN_IDENTITY': (None, '35'),
    'ITERATIONS': (None, '1'),
    'E_VALUE': (None, '0.001'),
    'Homolog_search_algorithm': (None, 'BLAST'),
    'ALGORITHM': (None, 'Bayes'),
    'SUB_MATRIX': (None, 'BEST'),
    'JOB_TITLE': (None, ''),
    'user_email': (None, ''),
    'submitForm': (None, 'Submit')
}

print('Submitting job to Consurf...')
r = requests.post(url, files=files)

m = re.search('job number (\d+),', r.text)
if m:
    job_id = m.group(1)
    print('Submit successfully! Job ID: {0}'.format(job_id))
else:
    print('Submit failed')
    quit()

output_url = 'https://consurf.tau.ac.il/results/{0}/output.php'.format(job_id)

while True:
    print('Check Job Running Status: ')
    r = requests.get(output_url)
    m = re.search('in_progress', r.text)
    if not m:
        print('  DONE!')
        break
    else:
        print('  RUNNING...')
        time.sleep(3)

print('Fetch Results...')
zip_url = 'https://consurf.tau.ac.il/results/{0}/Consurf_Outputs_{0}.zip'.format(job_id)
download_file(zip_url, 'consurf.zip')

print('All done.')
