tmpfile=${RANDOM}.pym
cat <<! >$tmpfile
import re
pdb_file = '$1'
ind_file = '$2'
out_file = '$3'
cmd.load(pdb_file, 'test')
cmd.set('dot_solvent', 1)
f = open(out_file, 'w+')
n = 0
for line in open(ind_file):
  l = re.split('\\s+', line.strip())
  if len(l) != 0:
    chain = re.split('/', l[1])[1]
    resi = re.split('/', l[2])[0]
    sele = 'test and chain {} and resi {}'.format(chain, resi)

    area1 = cmd.get_area(sele)

    obj = 'res-{}'.format(n + 1)
    cmd.create(obj, sele)
    area2 = cmd.get_area(obj)

    f.write('{}\\n'.format(area1 / area2))
    n += 1
f.close()
cmd.quit()
!
pymol -c $tmpfile
rm -f $tmpfile
