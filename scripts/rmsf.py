import numpy as np
import MDAnalysis as mda
import matplotlib.pyplot as plt
import sys

trajfile = sys.argv[1]
txtfile = sys.argv[2]
figfile = sys.argv[3]

u = mda.Universe(trajfile)

ca = u.select_atoms("name CA")
means = np.zeros((len(ca), 3))
sumsq = np.zeros_like(means)

for k, ts in enumerate(u.trajectory):
#    print(k)
    sumsq += (k/(k+1.0)) * (ca.positions - means) ** 2
    means[:] = (k*means + ca.positions) / (k+1.0)
rmsf = np.sqrt(sumsq.sum(axis=1)/(k+1.0))

f = open(txtfile, 'w+')
f.write('\n'.join(str(i) for i in rmsf))
f.close()

plt.plot(ca.residues.resids, rmsf)
plt.xlabel('Residue')
plt.ylabel('RMSF')
#plt.show()
plt.savefig(figfile)
