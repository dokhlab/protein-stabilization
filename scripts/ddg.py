import glob
import sys
import re
import numpy as np
import math

def en_correction(muts, is_flex):
    aa = ['A','C','D','E','F','G','H','I','K','L', 'M','N','P','Q','R','S','T','V','W','Y']
#    fix_ref = [-2.2872575e-01, -13.7526, 8.2699005e-01, 8.9655130e-01, -6.4080439e-01, 1.1586282e+00, -5.1355324e-01, -1.9323874e+00, 1.9385293e+00, -1.5957247e+00, -3.2601997e+00, -1.6292387e-01, 1.5895277e+00, 4.6416330e-01, -9.3789996e-01, 1.0668772e+00, 1.5412980e-01, -1.7878265e+00, 2.4509595e+00, -2.4761284e+00]
#    flex_ref = [-2.0974822e-01, -12.8622, 8.9327116e-01, 7.4950170e-01, -9.4470544e-01, 9.2032432e-01, -2.0916426e-02, -1.4307071e+00, 1.9928389e+00, -1.0490696e+00, -2.3697980e+00, 5.1998698e-02, 1.7830848e+00, 3.7188483e-01, -6.6247947e-01, 7.2290444e-01, 1.4270730e-01, -1.3768622e+00, 1.4537314e+00, -2.3547098e+00]
    fix_ref = [-2.2872575e-01, -13.7526, 8.2699005e-01, 8.9655130e-01, -6.4080439e-01, 1.1586282e+00, -5.1355324e-01, -1.9323874e+00, 1.9385293e+00, -1.5957247e+00, -3.2601997e+00, -1.6292387e-01, -1.5895277e+00, 4.6416330e-01, -9.3789996e-01, 1.0668772e+00, 1.5412980e-01, -1.7878265e+00, 2.4509595e+00, -2.4761284e+00]
    flex_ref = [-2.0974822e-01, -12.8622, 8.9327116e-01, 7.4950170e-01, -9.4470544e-01, 9.2032432e-01, -2.0916426e-02, -1.4307071e+00, 1.9928389e+00, -1.0490696e+00, -2.3697980e+00, 5.1998698e-02, -1.7830848e+00, 3.7188483e-01, -6.6247947e-01, 7.2290444e-01, 1.4270730e-01, -1.3768622e+00, 1.4537314e+00, -2.3547098e+00]
    ref = (flex_ref if is_flex else fix_ref)
    muts = re.split(',', muts)
    correction = 0
    for mut in muts:
        m = re.match('([A-Z])(\d+)([A-Z])', mut)
        if m:
            aao = m.group(1)
            aan = m.group(3)
            try:
                ind_o = aa.index(aao)
                ind_n = aa.index(aan)
                correction -= ref[ind_o]
                correction += ref[ind_n]
            except:
                pass
    return correction

def min_en(filename):
    mn = sys.float_info.max
    v = []
    for line in open(filename):
        if line.startswith('ATOM'):
#            return v[-1]
            return np.min(v)
        else:
            l = re.split('\s+', line.strip())
            e = float(l[3])
            v.append(e)
#            if e < mn:
#                mn = e

muts = sys.argv[1]
is_flex = 0
if len(sys.argv) >= 3:
    is_flex = int(sys.argv[2])

correction = en_correction(muts, is_flex)
en_nat = []
en_mut = []
for i in glob.glob('eris_working/{}_*'.format(muts)):
    for j in glob.glob('{}/nat/nat.run*'.format(i)):
        en_nat.append(min_en(j))
    for j in glob.glob('{}/mut/mut.run*'.format(i)):
        en_mut.append(min_en(j))
en_nat = np.array(en_nat)
en_mut = np.array(en_mut)
#mean_nat = np.mean(en_nat)
#mean_mut = np.mean(en_mut)
mean_nat = np.min(en_nat)
mean_mut = np.min(en_mut)
std_nat = np.std(en_nat)
std_mut = np.std(en_mut)
#print(mean_nat, std_nat, mean_mut, std_mut, mean_mut-mean_nat+correction, math.sqrt(std_nat*std_nat+std_mut*std_mut))
print('{5:.3f} {6:.3f}'.format(mean_nat, std_nat, mean_mut, std_mut, mean_mut-mean_nat, mean_mut-mean_nat+correction, math.sqrt(std_nat*std_nat+std_mut*std_mut)))
